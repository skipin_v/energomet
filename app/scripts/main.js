// $(window).on('load resize', function() {
//     $('.responsive-slider').responsiveSlider({
//         autoplay: true,
//         interval: 6000,
//         transitionTime: 1200,
//         animate: 'ease'
//     });
// });

$(document).on('af_complete', function(event, response) {
    if (response.success) { //Если форма заполнена и нет ошибок
        $('#clientForm').modal('hide');
    }
});

$(document).ready(function() {
    $('.responsive-slider').responsiveSlider({
        autoplay: true,
        interval: 6000,
        transitionTime: 1200,
        animate: 'ease'
    });
    $('.certifitates-slider').lightSlider({
        item: 5,
        autoWidth: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 10,

        prevHtml: '<span class="glyphicon glyphicon-chevron-left"></span>',
        nextHtml: '<span class="glyphicon glyphicon-chevron-right"></span>',

        addClass: '',
        mode: 'slide',
        useCSS: true,
        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
        easing: 'linear', //'for jquery animation',////

        speed: 400, //ms'
        auto: false,
        loop: true,
        pager: false,
        slideEndAnimation: true,
        pause: 2000,
        responsive: [{
                breakpoint: 1280,
                settings: {
                    item: 5,
                    // slideMove:1,
                    // slideMargin:6,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    item: 4,
                    // slideMove:1,
                    // slideMargin:6,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    item: 3,
                    // slideMove:1,
                    // slideMargin:6,
                }
            },
            {
                breakpoint: 680,
                settings: {
                    item: 2,
                    // slideMove:1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 1,
                    // slideMove:1
                }
            }
        ]
    });

    // Material label for input
    $('.material').focusin(function() {
        $(this).parent().addClass('active');
    });
    $('.material').focusout(function() {
        if ($(this).val().length > 0) {
            return;
        } else {
            $(this).parent().removeClass('active');
        }
    });

    // Phone number
    $('.form-phone').mask('+7 (000) 000-00-00');
    $.validate({
        form: 'form',
        modules: 'toggleDisabled',
        // addSuggestions: false,
        // showHelpOnFocus: false,
        // disabledFormFilter: 'form.toggle-disabled',
        validateOnBlur: false
    });

});

// Maps
function GoogleMapsInitialize() {
    // var map = new google.maps.LatLng(parseFloat(trader.geo.lat),parseFloat(trader.geo.lon));
    var mapLatLng = { lat: 56.781978, lng: 60.618001 },
        markerLatLng = { lat: 56.781978, lng: 60.618001 }
    var styles = [{
        'featureType': 'landscape.man_made',
        'elementType': 'all',
        // 'stylers': [
        //     { 'saturation': 28 },
        //     { 'lightness': -17 },
        //     { 'gamma': 0.75 }
        // ]
    }];
    var mapOptions = {
        scrollwheel: false,
        zoom: 16,
        center: mapLatLng,
        disableDefaultUI: true,
        // mapTypeId: google.maps.MapTypeId.ROADMAP,
        // styles: styles
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var image = new google.maps.MarkerImage('images/elements/Contacts/icon_geo.png', new google.maps.Size(42, 68));
    var marker = new google.maps.Marker({
        position: markerLatLng,
        map: map,
        icon: image,
        animation: google.maps.Animation.DROP,
        title: 'ООО ЭнергоМет'
    });
};