// generated on 2017-01-24 using generator-webapp 2.3.2
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var dev = true;

gulp.task('styles', () => {
    return gulp.src('app/styles/*.{sass,scss}')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer())
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(reload({ stream: true }));
});

// gulp.task('vendors_css', () => {
//     return gulp.src([
//             'app/styles/vendors/*.css',
//             // 'bower_components/',
//         ])
//         .pipe($.plumber())
//         .pipe($.cssnano({ safe: true, autoprefixer: false, discardEmpty: true }))
//         .pipe($.concat('external-vendors.css').on('error', $.util.log))
//         // .pipe(gulpif(env === 'production', uglifyCss()))
//         .pipe(gulp.dest('.tmp/styles'))
//         // .pipe($.notify({ message: 'CSS вендоры объединены!' })
//     ;
// });

gulp.task('scripts', () => {
    // return gulp.src('app/scripts/**/*.js')
    return gulp.src('app/scripts/**/*.{js,coffee}')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        // .pipe($.babel())
        .pipe($.if('*.js', $.babel(), $.coffee()))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp/scripts'))
        .pipe(reload({ stream: true }));
});
// gulp.task('scripts:js', () => {
//     // return gulp.src('app/scripts/**/*.js')
//     return gulp.src('app/scripts/**/*.js')
//         .pipe($.plumber())
//         .pipe($.sourcemaps.init())
//         // .pipe($.babel())
//         .pipe($.if('.js', $.babel(), $.coffee()))
//         .pipe($.sourcemaps.write('.'))
//         .pipe(gulp.dest('.tmp/scripts'))
//         .pipe(reload({ stream: true }));
// });

// gulp.task('vendors_js', () => {
//   return gulp.src([
//       'app/scripts/vendors/*.js',
//       // 'bower_components/',
//     ])
//     .pipe($.concat('vendors_plugin.js').on('error', $.util.log))
//       // .pipe(gulpif(env === 'production', uglifyCss()))
//     .pipe(gulp.dest('.tmp/scripts'));
// });

gulp.task('scripts:test', () => {
    return gulp.src('test/spec/**/*.coffee')
        .pipe($.plumber())
        .pipe($.coffee())
        .pipe(gulp.dest('.tmp/spec'))
        .pipe(reload({ stream: true }));
});

gulp.task('views', () => {
    return gulp.src('app/*.pug')
        .pipe($.plumber())
        .pipe($.pug({
            pretty: true
        }))
        .pipe(gulp.dest('.tmp'))
        .pipe(reload({ stream: true }));
});

function lint(files, options) {
    return gulp.src(files)
        .pipe($.eslint({ fix: true }))
        .pipe(reload({ stream: true, once: true }))
        .pipe($.eslint.format())
        .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
    return lint('app/scripts/**/*.js')
        .pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
    return lint('test/spec/**/*.js')
        .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['views', 'styles', 'scripts'], () => {
    // return gulp.src('app/*.html')
    return gulp.src(['app/*.html', '.tmp/*.html'])
        .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
        .pipe($.if('*.js', $.uglify()))
        // .pipe($.if('*.css', $.cssnano({ safe: true, autoprefixer: false })))
        .pipe($.if('*.css', $.cssnano({ safe: true, autoprefixer: true })))
        .pipe($.if('*.html', $.htmlmin({ collapseWhitespace: true })))
        .pipe(gulp.dest('dist'));
});

gulp.task('images', () => {
    return gulp.src('app/images/**/*')
        .pipe($.cache($.imagemin()))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', () => {
    return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function(err) {})
            .concat('app/fonts/**/*'))
        .pipe($.if(dev, gulp.dest('.tmp/fonts'), gulp.dest('dist/fonts')));
});

gulp.task('extras', () => {
    return gulp.src([
        'app/*.*',
        '!app/*.html',
        '!app/*.pug'
    ], {
        dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', () => {
    runSequence(['clean', 'wiredep'], ['views', 'styles', 'scripts', 'fonts'], () => {
        browserSync.init({
            notify: false,
            port: 9000,
            server: {
                baseDir: ['.tmp', 'app'],
                routes: {
                    '/bower_components': 'bower_components'
                }
            }
        });

        gulp.watch([
            'app/*.html',
            'app/images/**/*',
            '.tmp/fonts/**/*'
        ]).on('change', reload);

        gulp.watch('app/**/*.pug', ['views']);
        gulp.watch('app/styles/**/*.{sass,scss}', ['styles']);
        // gulp.watch('app/scripts/**/*.js', ['scripts']);
        gulp.watch('app/scripts/**/*.{js,coffee}', ['scripts']);
        gulp.watch('app/fonts/**/*', ['fonts']);
        gulp.watch('bower.json', ['wiredep', 'fonts']);
    });
});

gulp.task('serve:dist', ['default'], () => {
    browserSync.init({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['dist']
        }
    });
});

gulp.task('serve:test', ['scripts', 'scripts:test'], () => {
    browserSync.init({
        notify: false,
        port: 9000,
        ui: false,
        server: {
            // baseDir: 'test',
            baseDir: ['.tmp', 'test'],
            routes: {
                '/scripts': '.tmp/scripts',
                '/bower_components': 'bower_components'
            }
        }
    });

    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('test/spec/**/*.coffee', ['scripts:test']);
    gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
    gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
gulp.task('wiredep', () => {
    gulp.src('app/styles/*.{sass,scss}')
        .pipe($.filter(file => file.stat && file.stat.size))
        .pipe(wiredep({
            ignorePath: /^(\.\.\/)+/
        }))
        .pipe(gulp.dest('app/styles'));

    // gulp.src('app/*.html')
    gulp.src('app/layouts/*.pug')
        .pipe(wiredep({
            exclude: ['bootstrap-sass'],
            ignorePath: /^(\.\.\/)*\.\./
        }))
        // .pipe(gulp.dest('app'));
        .pipe(gulp.dest('app/layouts'));
});

gulp.task('gzip', ['build'], () => {
    return gulp.src('dist/**/*.{xml,json,css,js,svg,ttf,otf,eot,woff,woff2}')
        .pipe($.gzip({
            append: true,
            gzipOptions: { level: 9, memLevel: 9 }
        }))
        .pipe(gulp.dest('dist'));
});

// gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
gulp.task('build', ['lint', 'html', 'fonts', 'extras'], () => {
    return gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true }));
});

gulp.task('default', () => {
    return new Promise(resolve => {
        dev = false;
        // runSequence(['clean', 'wiredep'], 'build', resolve);
        runSequence(['clean', 'wiredep'], 'gzip', resolve);
    });
});
